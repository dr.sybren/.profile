function gitgone -d "List or remove branches whose upstream branch is gone"
    argparse h/help D/delete -- $argv
    or return 47

    if set -ql _flag_help
        echo "gitgone [-h|--help] [-D|--delete]"
        echo
        echo "gitgone lists all Git branches for which their upstream tracking branch is gone."
        echo
        echo "--delete  instead of listing, the branches, delete them."
        return 0
    end

    if set -ql _flag_delete
        # Use the 'list' functionality of this function to delete the branches.
        set -l BRANCHES (gitgone)
        if test -z "$BRANCHES"
            echo "No 'gone' branches."
            return 0
        end

        git branch -D $BRANCHES
        return 0
    end

    # List the 'gone' branches,
    git branch -vv | awk '/: gone]/{ print $1 }'
end
