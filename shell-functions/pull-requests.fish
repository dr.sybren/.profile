###
# Two functions for the Fish shell, to deal with pull requests.
###

# Pull in a pull request, and merge it with its base branch.
#
# Example for Blender:        pr 12345
# Example for Blender manual: pr -r blender-manual 12345
# Example for Flamenco:       pr -r studio/flamenco 12345
function pr -d "Pull in a Blender pull request"
    # Set defaults
    set _flag_owner blender
    set _flag_repo blender

    argparse 'o/owner=' 'r/repo=' -- $argv
    or return 48

    # Check for -r owner/repo notation:
    set _flag_combined (echo "$_flag_repo" | string split /)
    set _num_combined (count $_flag_combined)
    if [ $_num_combined -gt 1 ]
        set _flag_owner $_flag_combined[1]
        set _flag_repo $_flag_combined[2]
    end

    set PRNUM $argv[1]

    set JSON "pr-$PRNUM.json"
    set URL "https://projects.blender.org/api/v1/repos/$_flag_owner/$_flag_repo/pulls/$PRNUM"
    if ! curl --fail --no-progress-meter -o$JSON $URL
        printf "\033[91mError fetching $URL\033[0m\n"
        return 49
    end

    # Sanity check we can read the result
    if ! jq <$JSON >/dev/null
        printf "\033[91mCannot parse JSON at $JSON\033[0m\n" >&2
        return 50
    end

    set PR_STATUS (jq -r .state <$JSON)
    if [ (jq -r .merged <$JSON) = "true" ]
        printf "\033[92mPR is already merged, check $URL\033[0m\n"
        echo "Merged at:" (jq -r .merged_at <$JSON)
        echo "Merged by:" (jq -r .merged_by.full_name <$JSON)
        return 51
    end
    if [ (jq -r .state <$JSON) = "closed" ]
        printf "\033[91mPR is closed, check $URL\033[0m\n"
        return 50
    end

    set PR_NUM (jq -r .number <$JSON)
    set PR_TITLE (jq -r .title <$JSON)
    printf "\033[95mPulling #$PR_NUM: $PR_TITLE\033[0m\n"

    set PR_REPO (jq -r .head.repo.full_name <$JSON)   # "ChrisLend/blender"
    set PR_AUTHOR (jq -r .head.repo.owner.username <$JSON)   # "ChrisLend"
    set PR_BRANCH (jq -r .head.ref <$JSON) # "vertical_scrolling_offscreen"
    set PR_BASE_BRANCH (jq -r .base.ref <$JSON) # "main" or "blender-v3.5-release"
    set LOCAL_BRANCH "PR/$PRNUM/$PR_AUTHOR-$PR_BRANCH"

    printf "\033[90mIncoming branch: $PR_REPO\033[0m\n"
    printf "\033[90mBase  branch   : $PR_BASE_BRANCH\033[0m\n"
    printf "\033[90mLocal branch   : $LOCAL_BRANCH\033[0m\n"

    rm -f $JSON

    set CURBRANCH (git branch --show-current 2>/dev/null)
    if test "$CURBRANCH" != "$PR_BASE_BRANCH";
        git checkout $PR_BASE_BRANCH
    end
    if git branch | grep -q $LOCAL_BRANCH;
        printf "\033[95mBranch $LOCAL_BRANCH exists, going to refresh it\033[0m\n"
        git branch -D $LOCAL_BRANCH
    else
        printf "\033[96mBranch $LOCAL_BRANCH does not exist\033[0m\n"
    end

    git checkout -b $LOCAL_BRANCH $PR_BASE_BRANCH; or return 47
    git pull --no-rebase --ff --no-edit --commit https://projects.blender.org/$PR_REPO $PR_BRANCH; or return 48

    printf "\033[95mBranch $LOCAL_BRANCH is ready for use\033[0m\n"
    printf "\033[94mPulled #$PR_NUM: $PR_TITLE\033[0m\n"
end

# Remove all pull request branches created with the above command.
function prprune -d "Delete all PR/* branches"
    set BRANCHES (string trim (git branch | grep '^  PR/'))
    if test -z "$BRANCHES";
        echo "No PR branches to prune"
        return 0
    end
    git branch -D $BRANCHES
end
