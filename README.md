<table>
    <tr><td><strong>Blender Chat</strong></td><td>@dr.sybren</td></tr>
    <tr><td><strong>Email       </strong></td><td>sybren@blender.org</td></tr>
    <tr><td><strong>Mastodon    </strong></td><td><a href='https://mastodon.art/@sybren'>@sybren@mastodon.art</a></td></tr>
    <tr><td><strong>Weekly Reports</strong></td><td>
<a href='https://projects.blender.org/dr.sybren/.profile/src/branch/main/reports/2025.md'>2025</a>
/ <a href='https://projects.blender.org/dr.sybren/.profile/src/branch/main/reports/2024.md'>2024</a>
/ <a href='https://wiki.blender.org/wiki/User:Sybren/Reports/2023'>2023</a>
/ <a href='https://wiki.blender.org/wiki/User:Sybren/Reports/2022'>2022</a>
/ <a href='https://wiki.blender.org/wiki/User:Sybren/Reports/2021'>2021</a>
/ <a href='https://wiki.blender.org/wiki/User:Sybren/Reports/2020'>2020</a>
/ <a href='https://wiki.blender.org/wiki/User:Sybren/Reports/2019'>2019</a>
    </td></tr>
    <tr><td>Other stuff</td>
    <td>
        <a href="https://projects.blender.org/dr.sybren/.profile/src/branch/main/webbrowser-scripts/README.md">User CSS and JS scripts</a>
        / <a href="https://projects.blender.org/dr.sybren/.profile/src/branch/main/blender-snippets/">Blender code snippets</a>
        / <a href="https://projects.blender.org/dr.sybren/.profile/src/branch/main/shell-functions/">Shell snippets</a>
    </td></tr>
</table>


# About me

I'm a Blender developer, employed by Blender Institute, project lead of **Animation 2025**, and **module owner of the [Animation & Rigging module](https://projects.blender.org/blender/blender/wiki/Module:%20Animation%20&%20Rigging)**. I'm also the **main developer of [Flamenco](https://projects.blender.org/studio/flamenco)**, and the author of [Scripting for Artists](https://studio.blender.org/training/scripting-for-artists/).

I obtained my PhD in 2016 at the Virtual Worlds group, Utrecht University, on the subject of **the animation of virtual characters in very dense crowds**. I obtained my MSc degree in computer science (Game & Media Technology) at Utrecht University, in 2010. My thesis subject was a solution to the stepping stone problem: the generation of walking motion from pre-recorded motion capture data, such that the feet step exactly at the required positions. You can find [my theses and other publications](http://stuvel.eu/publications) on my website.

## Creative work

Apart from all the computer-related stuff, I'm [a photographer](http://stuvelfoto.nl/), and mostly shoot either [black & white portraits](http://stuvelfoto.nl/portret) or create [spherical panoramas](http://stuvelfoto.nl/panorama). I'm also the drummer in the Dutch band [The Soundabout](http://soundabout.nl/).
