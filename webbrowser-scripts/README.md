# Webbrowser Scripts

These are JS scripts and CSS files that are meant to locally enhance websites.

## JavaScript

These scripts require [Greasemonkey for Firefox][GMFF] or
[Tampermonkey for Chrome/Chromium-based browsers][GMChrome].

[gitea-document-links.js](https://projects.blender.org/dr.sybren/.profile/raw/branch/main/webbrowser-scripts/gitea-document-links.js)
: Adds buttons to [Gitea][gitea] to copy a link to the currently shown
  issue/pull request/commit. Based on [my 2023 blog post][blogpost].

[GMFF]: https://www.greasespot.net/
[GMChrome]: https://www.tampermonkey.net
[gitea]: https://projects.blender.org/
[blogpost]: https://stuvel.eu/post/2023-02-09-gitea-clipboard-buttons/

## CSS

These files are meant for use with [Stylus for Firefox][StylusFF] or [User
JavaScript and CSS for Chrome/Chromium-based browsers][UserCSSChrome].

[gitea-sybren-style.css](https://projects.blender.org/dr.sybren/.profile/raw/branch/main/webbrowser-scripts/gitea-sybren-style.css)
: Slight style tweak for [Gitea][gitea]. Mostly for me to highlight the Animation & Rigging module in a few places.

[blender-chat-sybren-style.css](https://projects.blender.org/dr.sybren/.profile/raw/branch/main/webbrowser-scripts/blender-chat-sybren-style.css)
: Style tweaks to make [Blender Chat][bchat] better readable. At least for my eyes.

[StylusFF]: https://addons.mozilla.org/en-US/firefox/addon/styl-us/
[UserCSSChrome]: https://chromewebstore.google.com/detail/nbhcbdghjpllgmfilhnhkllmkecfmpld
[bchat]: https://blender.chat/
